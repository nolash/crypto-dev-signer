class UnknownAccountError(Exception):
    pass


class TransactionRevertError(Exception):
    pass


class NetworkError(Exception):
    pass
